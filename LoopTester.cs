using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;

namespace LoopTesting
{
    public class LoopTester
    {
        private readonly int[] _data;
        private readonly ulong _dataSize;
        private readonly Func<int[], ulong, int[]> _executedLoop;
        private readonly ulong _loopRange;
        private readonly ulong _numberOfLoopExecutions;
        private readonly Queue<long> _loopExecutionDurations;

        public LoopTester(Func<int[], ulong, int[]> executedLoop, ulong numberOfLoopExecutions, ulong loopRange,
            ulong dataSize)
        {
            _executedLoop = executedLoop;
            _numberOfLoopExecutions = numberOfLoopExecutions;
            _loopRange = loopRange;
            _dataSize = dataSize;
            _data = new int[dataSize];
           
            var lines = File.ReadAllLines("/home/blacky/Documents/Random.txt");
            for (var i = 0; i < lines.Length; i++) {
                var line = lines[i];
                _data[i] = Int32.Parse(line);
            }
            _loopExecutionDurations = new Queue<long>();
        }

        public void RunTest()
        {
            var watch = new Stopwatch();
            for (ulong i = 0; i < _numberOfLoopExecutions; i++)
            {
                watch.Restart();
                _executedLoop(_data, _loopRange);
                watch.Stop();
                _loopExecutionDurations.Enqueue(watch.ElapsedTicks);
            }
        }

        public double GetAverageTimeInNanoseconds()
        {
            var timesNanoseconds = new List<double>();
            foreach (var span in _loopExecutionDurations)
            {
                timesNanoseconds.Add(span/(1.0*Stopwatch.Frequency)*1000000000);
            }

            return timesNanoseconds.Average();
        }
        
        public double GetAverageTimeInMilliseconds()
        {
            var tickSpanList =_loopExecutionDurations.ToList();
            var timesNanoseconds = new List<long>();
            foreach (var span in tickSpanList)
            {
                timesNanoseconds.Add(span/Stopwatch.Frequency*1000);
            }

            return timesNanoseconds.Average();
        }

        public void DescribeHarvestedData()
        {
            var output =
                "Loop executions: " + _numberOfLoopExecutions +
                "Loop range: " + _loopRange +
                "Data size: " + _dataSize +
                "Average time: " +
                GetAverageTimeInNanoseconds().ToString("e") + " [ns]";
            
            Console.WriteLine(output);
        }

        public string GetCsvInformation()
        {
            var result = string.Format("{0:D},{1:D},{2:D},{3:F4}", 
            _numberOfLoopExecutions, 
            _loopRange, 
            _dataSize, 
            GetAverageTimeInNanoseconds());

            return result;
        }
    }
}