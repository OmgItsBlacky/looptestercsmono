using System;
using System.Collections.Generic;
using System.IO;

namespace LoopTesting
{
    public class CSVFileGenerator
    {
        private const string DefaultFileName = "CS_LOOP_TESTER_OUTPUT_";

        private const string FileHeader = "Loop executions," +
                                          "Loop range,Data size," +
                                          "Average time of single loop execution";
                                     
        private static string PathAdjuster(string path)
        {
            path += DefaultFileName;
            path += DateTime.Now.ToString("U").Replace(" ", "_").Replace(",", "");
            return path;
        }

        public CSVFileGenerator(string path, IEnumerable<LoopTester> testers)
        {
            var newPath = PathAdjuster(path);
            
            if (!File.Exists(newPath))
            {
                File.Create(newPath).Dispose();
            }

            if (File.Exists(newPath))
            {
                using (var tw = new StreamWriter(newPath))
                {
                    tw.WriteLine(FileHeader);
                    foreach (var tester in testers)
                    {
                        tw.WriteLine(tester.GetCsvInformation());
                    }
                }
            }
        }
    }
}