﻿using System;

namespace LoopTesting
{
    internal class TestingArea
    {
        public static int[] LongForLoopBubbleA(int[] data, ulong loopRange)
        {
            ulong i = 0;
            ulong j = 0;
            int temporary = 0;
        
            for (; i < loopRange; i++)
            {
                for (j = 0; j < loopRange - i; j++)
                {
                    if(data[j] > data[j+1])
                    {
                        temporary = data[j];
                        data[j] = data[j+1];
                        data[j+1] = temporary;
                    }
                }
            }
        
            return data;
        } 
        public static int[] LongForLoopBubbleB(int[] data, ulong loopRange)
        {
            ulong i = 0;
            ulong j = 0;
            int temporary = 0;
        
            for (; i < loopRange; i++)
            {
                for (j = 0, temporary = 0; j < loopRange - i; j++)
                {
                    if(data[j] > data[j+1])
                    {
                        temporary = data[j];
                    }
        
                    if(data[j] > data[j+1])
                    {
                        data[j] = data[j+1];
                    }
        
                    if(temporary > data[j+1])
                    {
                        data[j+1] = temporary;
                    }
                }
            }
        
            return data;
        }
 
        public static int[] LongForLoopBubbleC(int[] data, ulong loopRange)
        {
            ulong i = 0;
            ulong j = 0;
            int temporary = 0;
        
            for (; i < loopRange; i++)
            {
                for (j = 0, temporary = 0; j < loopRange - i; j++)
                {
                    temporary = data[j] > data[j+1] ? data[j] : temporary;
                    data[j] = data[j] > data[j+1] ? data[j+1] : data[j];
                    data[j+1] = temporary > data[j+1] ?  temporary : data[j+1];
                }
            }
        
            return data;
        }
 
        public static int[] LongForLoopBubbleD(int[] data, ulong loopRange)
        {
            ulong i = 0;
            ulong j = 0;
        
            int temporary_A = 0;
            int temporary_B = 0;
        
            for (; i < loopRange; i++)
            {
                for (j = 0; j < loopRange - i; j++)
                {
                    temporary_A = data[j];
                    temporary_B = data[j+1];
        
                    data[j] = temporary_B < temporary_A ? temporary_B : temporary_A;
                    data[j+1] = temporary_B < temporary_A ? temporary_A : temporary_B;
                }
            }
        
            return data;
        }

        public static void Main(string[] args)
        {
            const ulong dataSize = 100000;
            const ulong numberOfLoopExecutions = 1;
            
            var loopTesters = new LoopTester[]
            {
                new LoopTester(LongForLoopBubbleD, numberOfLoopExecutions, dataSize-1, dataSize)               
            };

            foreach (var loopTester in loopTesters)
            {
                loopTester.RunTest();
                loopTester.DescribeHarvestedData();
            }                        
        }
    }
}